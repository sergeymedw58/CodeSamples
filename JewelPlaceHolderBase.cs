using UnityEngine;
using Zenject;


    public class JewelPlaceHolderBase : MonoBehaviour
    {
        [SerializeField]
        protected JewelItemBase _jewel;
        [SerializeField]
        protected BoolEvent _movement;
        [SerializeField]
        private GameObject _zone;

        protected MetaBackpack _backpack;

        [Inject]
        private void Construct(MetaBackpack backpack)
        {
            _backpack = backpack;
            _jewel.OnJewelInBackpackEvent += () =>
            {
                _backpack.Add(_jewel);
            };
        }

        protected virtual void Start()
        {
            _jewel.Initialize();
            if(_jewel.IsCollected)
            {
                _zone.SetActive(false);
                _backpack.Add(_jewel);
                JewelItemBase.OnJewelCollected?.Invoke();
            }
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            GetJewel();   
        }

        protected void GetJewel()
        {
            if (!_jewel.IsCollected)
            {
                _zone.SetActive(false);
                _movement.SetActivityStateTo(false);
                _jewel.OnJewelInBackpackEvent += InputOn;
                _jewel.MoveToBackpack();
            }
        }

        private void InputOn()
        {
            _movement.SetActivityStateTo(true);
        }
    }

