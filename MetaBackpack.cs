using System;
using System.Collections.Generic;
using UnityEngine;


    public class MetaBackpack : MonoBehaviour
    {
        private Dictionary<string, ImetaItem> _metaItems = new Dictionary<string, ImetaItem>();
        public Dictionary<string, ImetaItem> MetaItems => _metaItems;

        public void Add<T>(T item) where T: ImetaItem
        {
            if( _metaItems.ContainsKey(item.ID))
            {
                throw new Exception("This key is already exist in backpack");
            }
            _metaItems[item.ID] = item;
        }

        public bool ContainsItem<T>( string id, out T item) where T: ImetaItem
        {
            item = default(T);
            if (_metaItems.ContainsKey(id))
            {
                item = (T)_metaItems[id];
                return true;
            }
            else
            {
                return false;
            }
        }
    }

