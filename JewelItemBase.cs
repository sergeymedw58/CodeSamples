using DG.Tweening;
using System;
using System.Collections;
using UnityEngine;
using Zenject;


    public class JewelItemBase : MonoBehaviour, ImetaItem
    {
        /// <summary>
        /// This event is called only for the end of the level
        /// </summary>
        public static Action OnJewelCollected;
        public Action OnJewelInBackpackEvent;
        #region Fields
        [SerializeField]
        private string _id;
        [SerializeField]
        private float _movingSpeed;
        [SerializeField]
        private Vector3 _angleForShowing;
        [SerializeField]
        private float _delayBeforeMoving;
        [SerializeField]
        private float _showTime;
        [SerializeField]
        private float _rotationSpeed;
        [SerializeField, Space]
        private Transform _viewPoint;
        [SerializeField]
        private GameObject _idleFX;
        [SerializeField]
        private GameObject _showingFX;

        private MetaItemSave _save = new MetaItemSave();
        private Coroutine _rotateRoutine;
        private MetaBackpack _backpack;
        private bool _isCollected = false;
        #endregion

        #region Properties
        public bool IsCollected => _isCollected;
        public string ID => _id;
        #endregion

        [Inject]
        private void Construct(MetaBackpack backpack)
        {
            _backpack = backpack;
            OnJewelInBackpackEvent += () =>
            {
                _isCollected = true;
                _save.Save(_id, _isCollected);
            };
        }

        public void Initialize()
        {
            _isCollected = _save.Load(_id);
            if (_isCollected)
            { 
                gameObject.transform.SetParent(_backpack.transform);
                gameObject.SetActive(false);
                gameObject.transform.localPosition = Vector3.zero;
            }
        }

        public void MoveToBackpack()
        {
            StartCoroutine(MoveToScreenRoutine());
            _rotateRoutine = StartCoroutine(RotateRoutine());
            gameObject.transform.SetParent(null);
        }

        private IEnumerator MoveToScreenRoutine()
        {
            yield return new WaitForSeconds(_delayBeforeMoving);
            _idleFX.SetActive(false);
            while (Vector3.Distance(_viewPoint.transform.position, gameObject.transform.position) > _movingSpeed * Time.deltaTime)
            {
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, _viewPoint.position, _movingSpeed * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }
            _showingFX.SetActive(true);
            yield return new WaitForSeconds(_showTime);
            StartCoroutine(MoveToHeroRoutine());
        }
        private IEnumerator RotateRoutine()
        {
            gameObject.transform.rotation = Quaternion.Euler(_angleForShowing);
            while(true)
            {
                gameObject.transform.RotateAround(gameObject.transform.up, _rotationSpeed);
                yield return new WaitForEndOfFrame();
            }
        }

        private IEnumerator MoveToHeroRoutine()
        {
            _showingFX.SetActive(false);
            while (Vector3.Distance(_backpack.transform.position, gameObject.transform.position) > _movingSpeed * Time.deltaTime)
            {
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, _backpack.transform.position, _movingSpeed * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }
            if (_rotateRoutine != null)
            {
                StopCoroutine(_rotateRoutine);
            }
            gameObject.transform.SetParent(_backpack.transform);
            gameObject.SetActive(false);
            OnJewelInBackpackEvent?.Invoke();
            OnJewelCollected?.Invoke();
            AnalyticsEventSender.ObjectiveReached(LevelManager.CurrentConfigLevel);
        }  
    }

